= WordPress をActivityPub 対応に
Kenichiro Matohara(matoken) <matoken@kagolug.org>
:revnumber: 1.0
:revdate: 2024-02-24(sat)
:revremark: 「{doctitle}」
:homepage: https://matoken.org/
:imagesdir: resources
:data-uri:
//:example-caption: 例
//table-caption: 表
//figure-caption: 図
:backend: revealjs
:revealjs_theme: solarized
:customcss: resources/my-css.css
:revealjs_slideNumber: c/t
:title-slide-transition: none
:icons: font
:revealjsdir: reveal.js/
:revealjs_hash: true
:revealjs_center: true
:revealjs_autoPlayMedia: true
:revealjs_transition: false
:revealjs_transitionSpeed: fast
:revealjs_plugin_search: enabled

== link:https://matoken.org[Kenichiro Matohara(matoken) https://matoken.org]

image::map.jpg[background, size=cover, width=80%]
* matoken @matoken@inari.opencocon.org
* 鹿児島の右下の山奥から参加
* 好きなLinuxディストリビューションはDebian

map: © OpenStreetMap contributors

== 前回

* 「TerminalでもFediverse」
** Terminal で動作するmisskey クライアント sayaka ちゃん, Mastodon クライアント toot の紹介  +
https://gitlab.com/matoken/fedilug-2023.12/-/blob/main/slide/slide.adoc
* その後
** Mastodon client link:https://github.com/taka-tuos/nanotodon[nanotodon] を試したり, shell browser に対応した ActivitiPyb サーバの link:https://epicyon.net/[Epicyon] を復活

=== nanotodon

image:nanotodon.jpg[width=60%]

=== Epicyon

image:epicyon.jpg[width=60%]

== WordPress を ActivityPub 対応に

image:WordPress-logotype-wmark.png[width=200]
image:ActivityPub-logo-symbol.svg[width=200]


== WordPress ActivityPub plugin

* link:https://wordpress.org/plugins/activitypub/[ActivityPub – WordPress plugin | WordPress.org]
** link:https://github.com/Automattic/wordpress-activitypub[Automattic/wordpress-activitypub: ActivityPub for WordPress]

=== テスト済

* Mastodon
* Pleroma/Akkoma
* friendica
* Hubzilla
* Pixelfed
* Socialhome
* Misskey
* Firefish (rebrand of Calckey)

== サブディレクトリに設置したblog なのでダメそうか……

* 初期の頃は http://blog.example.com のようなドメインやサブドメイン以下に WordPress が設置されている必要があった
* 今運用している WordPress は http://example.com/blog のような形式なのでNG

=== しばらく前に確認してみると

[quote, 'https://github.com/Automattic/wordpress-activitypub#what-if-you-are-running-your-blog-in-a-subdirectory']
____
What if you are running your blog in a subdirectory?
In order for webfinger to work, it must be mapped to the root directory of the URL on which your blog resides.
____

* 行けるようになってそう

== 導入

* WordPress の 管理者アカウントで「プラグイン」->「新規追加」から "activitypub" を検索，「インストール」ボタン  +
http://${BLOGURL}/wp-admin/network/plugin-install.php?s=activitypub&tab=search&type=term
* お手軽

=== サブディレクトリ対応

* 今回の blog は https://example.com/blog/ のようにサブディレクトリ以下に格納されているので webfinger の設定をしてあげる必要がある

.Apache httpd の場合，root ディレクトリ以下の .htaccess で以下を設定( `/blog/` 部分は実際のサブディレクトリに合わせる
----
RedirectMatch "^\/\.well-known/(webfinger|nodeinfo|x-nodeinfo2)(.*)$" /blog/.well-known/$1$2
----

NOTE: Nginx の例も https://github.com/Automattic/wordpress-activitypub?tab=readme-ov-file#what-if-you-are-running-your-blog-in-a-subdirectory

[.columns]
== 試してみる

[.column]
--
@ドメイン名@ドメイン名::
blog 全体の投稿の購読(頭のID名は変更可能)
@投稿アカウント名@ドメイン名::
* アカウント毎の投稿の購読
* アカウント名やプロフィールはblogの設定が反映される(プロフィールは Fediverse 用が設定可能)
--

[.column]
--
image:profile.jpg[]
--

=== Fediverse 用プロフィール

image:profile-setting.jpg[]

== リモートから

image:remote-domain.jpg[]
image:remote-user.jpg[]

== Mastdonからフォローした状態で新規 blog 記事を投稿

image:post01.jpg[width=20%]
image:post02.jpg[width=20%]

=== 投稿内容のカスタマイズ

image:post-setting.jpg[width=45%]

== おすすめプラグイン

image:favorit-plugins.jpg[width=40%]

=== !

* 他のユーザーをフォロー  +
自分の WordPress を使って Mastodon または同様のプラットフォームのユーザーをフォローするには、WordPress の Friends プラグインを使用します。このプラグインを使用すると、投稿を受信してそれを WordPress に表示でき、WordPress を独自の Fediverse インスタンスとして使用できます。  +
https://wordpress.org/plugins/friends/
* URL 短縮ツールを追加  +
Hum は WordPress のパーソナル URL 短縮ツールです。WordPress または別の場所でホストされているパーソナルなコンテンツの短縮 URL を生成します。  +
https://wordpress.org/plugins/hum/

=== !

* 高度な WebFinger サポート  +
WebFinger は、人や物の情報を URL で識別して発見できるプロトコルです。例えば、人に関する情報は、Acct と呼ばれる URL（電子メールアドレスのような URL）を介して発見できます。
ActivityPub プラグインには基本の WebFinger サポートが備わっています。詳細な設定オプション、他の Fediverse または IndieWeb のプラグインとの互換性が必要な場合は、WebFinger プラグインをインストールしてください。  +
https://wordpress.org/plugins/webfinger/

=== !

* ブログに関してさらなる情報を提供
NodeInfo は、分散型ソーシャルネットワークを運用するサーバーに関するメタデータを公開できる共通仕様のことです。この取り組みにより、分散型 SNS のユーザーに関する統計を得られるようになるほか、ユーザーが自分のニーズに最適なソフトウェアとサーバーを選択できるようにするツールを構築できるようになります。
この ActivityPub プラグインは最低限の NodeInfo エンドポイントを提供していますが、さらに設定を行う場合や、ほかの Fediverse プラグインとの互換性を持たせる必要がある場合は、NodeInfo プラグインをインストールしてください。  +
https://wordpress.org/plugins/nodeinfo/

[.columns]
== 問題

[.column]
--
* WordPress 管理画面でフォロワーが表示されない
// /wp-admin/options-general.php?page=activitypub&tab=followers
image:follower-ng.jpg[]
--

[.column]
--
* リモートからは見えている?
image:remote-user.jpg[width=70%]
--

== その他

* pull型のRSS(xml) に標準対応 -> /feed/
* JSON Feed なんてものも入れてみている -> /feed/json/
** link:https://wordpress.org/plugins/jsonfeed/[JSON Feed (jsonfeed.org) – WordPress plugin | WordPress.org]

== まとめ

* blog システムの WordPress に ActivityPub plugin を導入してお手軽に ActivityPub 対応
* 別途告知アカウントを用意しなくても WordPress 自体が ActivitiPub に直接投稿
* 投稿内容も細かくカスタマイズできそう
* (RSS から告知用アカントに投げるもでいいような気もする……)

== 奥付

発表::
* link:https://fedilug.connpass.com/event/308507/[第1回 Fediverse Linux User Group 勉強会]
発表者::
* link:https://matoken.org/[Kenichiro Matohara(matoken)]
利用ソフトウェア::
* link:https://github.com/asciidoctor/asciidoctor-reveal.js[Asciidoctor Reveal.js]
ライセンス::
* link:https://creativecommons.org/licenses/by/4.0/[CC BY 4.0]

